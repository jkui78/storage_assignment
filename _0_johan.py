import numpy as np




# example: 4 products and 4 locations. Pick frequency between 2 prods IS NOT MAXIMIZED, it is merely used to ESTIMATE distance that will be used to complete pick history. THEREFORE IT MUST BE MINIMIZED

# Weights matrix
W = np.array([[0, 1, 2, 3],
             [1, 0, 4, 5],
             [2, 4, 0, 6],
             [3, 5, 6, 0]])

# distmat between the 4 locations
D = np.array([[0, 1.1, 2.1, 3.1],
             [1.1, 0, 4.1, 5.1],
             [2.1, 4.1, 0, 6.1],
             [3.1, 5.1, 6.1, 0]])

# cand0 = [0, 1, 2, 3]  # product 0 -> loc 0; product 1 -> loc 1; product 2 -> loc 2, product 3 -> loc3
#
# # Cost is calculated as a matrix operation between all pairs in upper triangle
# cost_loc0 = 1 * 1.1 + 2 * 2.1 + 3 * 3.1
# cost_loc1 = 4 * 4.1 + 5 * 5.1
# cost_loc2 = 6 * 6.1
# total_cost = cost_loc0 + cost_loc1 + cost_loc2
#
# cand1 = np.array([3, 1, 0, 2])  # prod 3 -> loc 0; prod 1 -> loc 1; prod 0 -> loc 2; prod 2 -> loc 3
# cost_loc0 = 5 * 1.1 + 3 * 2.1 + 6 * 3.1
# cost_loc1 = 1 * 4.1 + 4 * 5.1
# cost_loc2 = 2 * 6.1
# total_cost_t = cost_loc0 + cost_loc1 + cost_loc2
# total_cost_t = 14 + 5 + 2
x = np.array([[1, 3, 0, 1]])  # OBS INDEX IS PRODUCT (UNIQUE), LOCATION IS NUMBER
total_cost2 = 0
# for i in range(0, len(cand1) - 1):  # ASSUMES SYMMETRIC MATRICES (otherwise just add an if else)
# 	cost_loc = 0
# 	for j in range(i, len(cand1) - 1):
# 		cost_loc += W[cand1[i], cand1[j + 1]] * D[i, j + 1]
# 	total_cost2 += cost_loc
#

z = np.zeros(x.shape[0])
for i in range(0, x.shape[0]):  # candidate assignments
	cand = x[i, :]
	cand_score = 0
	for j in range(0, len(cand) - 1):  # products
		cost_loc = 0
		for k in range(j, len(cand) - 1):  # weight and distance diffs to current loc
			w = W[j, k + 1]
			d = D[cand[j], cand[k + 1]]
			cost_loc += w * d
		cand_score += cost_loc
	z[i] = cand_score

print("z: " + str(z))







