# Import modules
import numpy as np
np.random.seed(6)
import time

# Import PySwarms
import pyswarms_mod as ps
from pyswarms_mod.utils.functions import single_obj as fx
from pyswarms_mod.utils.plotters import (plot_cost_history, plot_contour, plot_surface)
import matplotlib.pyplot as plt

from qap_function import quadratic_assignment_function
# jp = [1, 1, 2, 2, 3, 4]  # CURRENTLY NOT WORKING (NEEDS TO FLIP SO THAT PROD 0 -> LOC 1, PROD 1 -> LOC 1 etc.)
jp = [12,7,9,3,4,8,11,1,5,6,10,2]  # these are the locations of the products, i.e. product 12 -> location 0, product 1..
# jp = [2, 1, 3]

NUM_PARTICLES = 5

jp = np.asarray(jp) - 1  # ALWAYS?
# np.random.shuffle(jp)
jo_pos = np.array([jp] * NUM_PARTICLES)

options = {'c1': 0.1, 'c2': 0.9, 'w': 0.1, 'k': 2, 'p': 1}
# optimizer = ps.single.GlobalBestPSO(n_particles=5, dimensions=2, options=options)
optimizer = ps.binary.BinaryPSO(n_particles=NUM_PARTICLES, dimensions=len(jp), options=options, init_pos=jo_pos)

time0 = time.time()
cost, pos = optimizer.optimize(quadratic_assignment_function, iters=10)
print("\ntook: " + str(time.time() - time0) + " seconds") # 58



# plot_cost_history(cost_history=optimizer.cost_history)
# plt.show()

tt = 6