

-The optimization loop is in pyswarms_mod/discrete/binary
-reset in base/discrete/base_dsicrere
-generate_discrete_swarm in backend/generators
-jo_compute_velocity in "backend/operators"
-compute position in binary
-new candidates generated in binary: self.swarm.position = self._compute_position(self.swarm)



STRATEGY: =======================================
-2 important additions needed to standard QAP: 1. Surjective case + location capacities. 2. Congestion penalty.
 Both are carried out in the S repo.
-Makes sense to not start with surjective case since there is no dataset. Preventing illegal swaps should not be too
 difficult to add later, e.g.
-





EXTRA NOTES: ====================================
- how not to write readable code:
neighbor_matrix = np.array(
            [
                np.random.choice(
                    # Exclude i from the array
                    np.setdiff1d(np.arange(swarm.n_particles), np.array([i])),
                    k,
                    replace=False,
                )
                for i in range(swarm.n_particles)
            ]
        )