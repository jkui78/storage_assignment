import numpy as np
import pickle

def quadratic_assignment_function(x):
	"""
	OBS X NEEDS TO HAVE SAME LEN AS NUMBER OF PRODUCTS
	# ASSUMES SYMMETRIC W AND D MATRICES (otherwise just add an if else)
	:param x: a numpy array of particles positions
	:return: loss of the particles positions
	"""

	# SHOULD BE PASSED INTO FUNCTION INSTEAD
	with open('./files/W', 'rb') as f:
		W = pickle.load(f)

	with open('./files/D', 'rb') as f:
		D = pickle.load(f)

	# # # Weights matrix
	# W = np.array([[0, 1, 2, 3],
	#               [1, 0, 4, 5],
	#               [2, 4, 0, 6],
	#               [3, 5, 6, 0]])
	#
	# # distmat between the 4 locations
	# D = np.array([[0, 1.1, 2.1, 3.1],
	#               [1.1, 0, 4.1, 5.1],
	#               [2.1, 4.1, 0, 6.1],
	#               [3.1, 5.1, 6.1, 0]])

	# # # Weights matrix
	# W = np.array([[0, 5, 6],
	#               [5, 0, 3.6],
	#               [6, 3.6, 0]])
	#
	# # distmat between the 4 locations
	# D = np.array([[0, 10, 3],
	#               [10, 0, 6.5],
	#               [3, 6.5, 0]])

	z = np.zeros(x.shape[0])

	for i in range(0, x.shape[0]):  # candidate assignments
		cand = x[i, :]

		# THIS DOES NOT HAVE CAPABILITY TO HANDLE SURJECTIVE CASE

		# # USING FOR LOOP -----------------
		cand_score = 0
		for j in range(0, len(cand) - 1):  # products are the indexes, locs are the values
			cost_loc = 0
			for k in range(j, len(cand) - 1):  # weight and distance diffs to current loc
				w = W[cand[j], cand[k + 1]]  # weight between PRODUCTS
				d = D[j, k + 1]  # distance between LOCATIONS. THESE PROB HAVE TO BE FLIPPED IN SURJECTIVE CASE
				cost_loc += w * d
			cand_score += cost_loc
		z[i] = cand_score

		# # USING MATRICES (maybe 15% faster) ---------------
		# P = np.zeros((12, 12))
		# for u in range(0, 12):
		# 	P[cand[u], u] = 1  # permutation matrix
		# cand_score = np.sum(P@D@P.T * W) / 2
		# z[i] = cand_score

	return z