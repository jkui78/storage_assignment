import numpy as np

x = [0, 1, 2]

# 0-1: 6.5*5    1-2: 10*3.6   0-2: 3*6
a = 6.5*5
b = 10*3.6
c = 18
# print(str(a + b+ c))


D = np.array([[0, 5, 6],
              [5, 0, 3.6],
              [6, 3.6, 0]])

W = np.array([[0, 10, 3],
              [10, 0, 6.5],
              [3, 6.5, 0]])

cand = x
cand_score = 0
for j in range(0, len(cand) - 1):  # products are the indexes, locs are the values
	cost_loc = 0
	for k in range(j, len(cand) - 1):  # weight and distance diffs to current loc
		w = W[cand[j], cand[k + 1]]
		d = D[j, k + 1]
		cost_loc += w * d
	cand_score += cost_loc


print("Cands: " + str(cand_score))

aa = 289 * 2
ff = 6


P = np.zeros((3, 3))
for i in range(0, 3):
	P[x[i], i] = 1

cost = np.sum(P@D@P.T * W) / 2
print("cost2: " + str(cost))

gg = 5

#
# P = np.array([[0, 0, 1],
#               [0, 1, 0],
#               [1, 0, 0]])
