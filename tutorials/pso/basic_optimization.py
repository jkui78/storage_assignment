
# Import modules
import numpy as np
# Import PySwarms
import pyswarms_mod as ps
from pyswarms_mod.utils.functions import single_obj as fx

#
# # Set-up hyperparameters
# options = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
#
# # Call instance of PSO
# optimizer = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options)
#
# # Perform optimization
# cost, pos = optimizer.optimize(fx.sphere, iters=1000)
#
# # WITH BOUNDS ====================================

# Create bounds
max_bound = 5.12 * np.ones(2)
min_bound = - max_bound
bounds = (min_bound, max_bound)

# Initialize swarm
options = {'c1': 0.5, 'c2': 0.3, 'w':0.9}

# Call instance of PSO with bounds argument
optimizer = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options, bounds=bounds)

# Perform optimization
cost, pos = optimizer.optimize(fx.sphere, iters=1000)

