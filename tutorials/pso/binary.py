# Import modules
import numpy as np

import pyswarms_mod as ps
from pyswarms_mod.utils.functions import single_obj as fx
from pyswarms_mod.utils.plotters import (plot_cost_history, plot_contour, plot_surface)
import matplotlib.pyplot as plt

options = {'c1': 0.1, 'c2': 0.9, 'w': 0.1, 'k': 2, 'p': 1}
# optimizer = ps.single.GlobalBestPSO(n_particles=5, dimensions=2, options=options)
optimizer = ps.binary.BinaryPSO(n_particles=5, dimensions=4, options=options)

cost, pos = optimizer.optimize(fx.rastrigin, iters=5)


# plot_cost_history(cost_history=optimizer.cost_history)
# plt.show()

tt = 6