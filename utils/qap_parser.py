
import numpy as np
import pickle

class QAP_parser:

	def __init__(self, PATH):
		# with open(PATH, 'r') as f:
		# 	self.the_file = f.readlines()

		with open(PATH) as f:  # removes line breaks
			self.the_file = f.read().splitlines()

		N = int(self.the_file[0])

		# MANUAL WAY FIRST TIME
		D = np.zeros((12, 12), dtype=int)
		for i in range(2, 14):  # distance matrix
			line = self.the_file[i].split()
			for j in range(0, 12):
				D[i - 2, j] = int(line[j])

		W = np.zeros((12, 12), dtype=int)
		for i in range(15, 27):  # weights matrix
			line = self.the_file[i].split()
			for j in range(0, 12):
				W[i - 15, j] = int(line[j])

		with open('./files/W', 'wb') as f:
			pickle.dump(W, f)

		with open('./files/D', 'wb') as f:
			pickle.dump(D, f)

		sol = (12,7,9,3,4,8,11,1,5,6,10,2)
		opt_dist = 578  # 289
		ddf = 5


if __name__ == "__main__":
	PATH = './files/test.txt'
	_m = QAP_parser(PATH)